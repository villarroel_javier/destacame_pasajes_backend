# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your views here.
from .models import Ticket
from rest_framework import viewsets
from .serializers import TicketSerializer

class TicketViewSet(viewsets.ModelViewSet):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer