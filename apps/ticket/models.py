# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

#modelo Ticket
class Ticket(models.Model):
    passenger = models.ForeignKey('passenger.Passenger', on_delete=models.CASCADE)
    seat = models.ForeignKey('bus.Seat', on_delete=models.CASCADE)
    schedule = models.ForeignKey('journey.Schedule', related_name='ticket_schedule', on_delete=models.CASCADE)
    comment = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
