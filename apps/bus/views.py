# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your views here.
from .models import Bus, Driver, Seat, Bus_Driver
from rest_framework import viewsets
from .serializers import BusSerializer, DriverSerializer, SeatSerializar, Bus_DriverSerializer

class BusViewSet(viewsets.ModelViewSet):
    queryset = Bus.objects.all()
    serializer_class = BusSerializer

    #al momento de crear un bus, se agregan 10 asientos asociados
    def perform_create(self, serializer):
        bus = serializer.save(capacity=10)
        seats = (Seat(number=i, status='e', bus=bus) for i in range(1, 11))
        Seat.objects.bulk_create(seats)
    
class DriverViewSet(viewsets.ModelViewSet):
    queryset = Driver.objects.all()
    serializer_class = DriverSerializer

class BusDriverViewSet(viewsets.ModelViewSet):
    queryset = Bus_Driver.objects.all()
    serializer_class = Bus_DriverSerializer

class SeatViewSet(viewsets.ModelViewSet):
    serializer_class = SeatSerializar

    #se redefine el queryset en caso que se necesite filtrar los asientos de acuerdo al bus
    def get_queryset(self):
        queryset = Seat.objects.all()
        bus_driver = self.request.query_params.get('bus_driver', None)
        if bus_driver is not None:
            queryset = queryset.filter(bus__bus_driver_bus=bus_driver)
        return queryset