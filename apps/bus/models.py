# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

#modelo Bus
class Bus(models.Model):
    code = models.CharField(max_length=255)
    registration = models.CharField(max_length=255)
    capacity = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

#modelo Asiento
class Seat(models.Model):
    status_options = (
        ('e', 'Enabled'),
        ('d', 'Disabled'),
    )
    bus = models.ForeignKey(Bus, related_name='seat_bus', on_delete=models.CASCADE)
    schedules = models.ManyToManyField('journey.Schedule', through='ticket.Ticket')
    number = models.IntegerField()
    status = models.CharField(max_length=1, choices=status_options, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    #atributo para saber si un asiento esta ocupado para un determinado horario
    def is_ocuppied(self, schedule_id):
        return Seat.objects.filter(schedules=schedule_id, schedules__seat=self).count() > 0

#modelo Chofer
class Driver(models.Model):
    bus = models.ManyToManyField(Bus, through='Bus_Driver')
    name = models.CharField(max_length=255)
    lastname = models.CharField(max_length=255)
    age = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    #atributo del nombre completo del chofer
    def fullName(self):
        return "{} {}".format(self.name, self.lastname)

#modelo Bus_Driver
class Bus_Driver(models.Model):
    bus = models.ForeignKey(Bus, related_name='bus_driver_bus')
    driver = models.ForeignKey(Driver, related_name='bus_driver_driver')
    class Meta:
        unique_together = (("bus", "driver"),)