from .models import Bus, Seat, Driver, Bus_Driver
from rest_framework import serializers
from django.utils.translation import ugettext_lazy as _

#Serializadores
class BusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bus
        fields = ('id', 'code', 'registration', 'capacity')
        read_only_fields = ('capacity',)

class DriverSerializer(serializers.ModelSerializer):
    fullName = serializers.ReadOnlyField()
    class Meta:
        model = Driver
        fields = ('id', 'name', 'lastname', 'age', 'fullName')

class Bus_DriverSerializer(serializers.ModelSerializer):
    bus_code = serializers.CharField(read_only=True, source="bus.code")
    bus_registration = serializers.CharField(read_only=True, source="bus.registration")
    driver_name = serializers.CharField(read_only=True, source="driver.fullName")
    name = serializers.SerializerMethodField()
    class Meta:
        model = Bus_Driver
        fields = ('id', 'bus', 'driver', 'bus_code', 'bus_registration', 'driver_name', 'name')

    #nombre formado por el codigo del bus y nombre completo del chofer
    def get_name(self, obj):
        return "[{}] - {}".format(obj.bus.code, obj.driver.fullName())

    #validacion para que el bus tenga un solo chofer asignado
    def validate_bus(self, bus):
        exists = Bus_Driver.objects.filter(bus=bus).count() > 0
        if(exists):
            raise serializers.ValidationError("El bus ya tiene asignado un chofer. Por favor verifica tu informacion.")
        return bus

class SeatSerializar(serializers.ModelSerializer):
    ocuppied = serializers.SerializerMethodField()
    class Meta:
        model = Seat
        fields = ('id', 'bus', 'number', 'ocuppied')

    #obtenemos el estado de un asiento de acuerdo al horario solicitado
    def get_ocuppied(self, obj):
        schedule_id = self.context['request'].GET.get('schedule', None)
        if schedule_id is None:
            return False
        else:
            return obj.is_ocuppied(schedule_id)