from .models import Journey, City, Schedule
from rest_framework import serializers
from django.db.models import Avg, Count
from django.utils.translation import ugettext_lazy as _

#Serializadores
class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('id', 'name')

class JourneySerializer(serializers.ModelSerializer):
    fullName = serializers.ReadOnlyField()
    city_start = serializers.CharField(read_only=True, source="start.name")
    city_end = serializers.CharField(read_only=True, source="end.name")
    class Meta:
        model = Journey
        fields = ('id', 'start', 'end', 'city_start', 'city_end', 'fullName')
        #extendemos validador de las ciudades para modificar el mensaje de salida
        validators = [
            serializers.UniqueTogetherValidator(
                queryset=model.objects.all(),
                fields=('start', 'end'),
                message=_("El trayecto a ingresar ya existe.")
            )
        ]

class JourneyAverageSerializer(serializers.ModelSerializer):
    num_schedules = serializers.SerializerMethodField()
    num_passengers = serializers.SerializerMethodField()
    average_passenger = serializers.SerializerMethodField()
    city_start = serializers.CharField(read_only=True, source="start.name")
    city_end = serializers.CharField(read_only=True, source="end.name")
    class Meta:
        model = Journey
        fields = ('id', 'start', 'end', 'city_start', 'city_end',
                  'num_passengers', 'num_schedules', 'average_passenger')

    #numero de horarios asociados al trayecto
    def get_num_schedules(self, obj):
        return obj.schedule_journey.all().count()

    #numero de pasajeros asociados a los trayectos
    def get_num_passengers(self, obj):
        return obj.schedule_journey.all().aggregate(num_tickets=Count('ticket_schedule')).get('num_tickets')

    #promedio de pasajeros por trayecto
    def get_average_passenger(self, obj):
        average = obj.schedule_journey.annotate(num_tickets=Count('ticket_schedule'))\
            .aggregate(Avg('num_tickets')).get('num_tickets__avg')
        if average is None:
            return 0
        return average

class ScheduleSerializer(serializers.ModelSerializer):
    city_start = serializers.CharField(read_only=True, source="journey.start.name")
    city_end = serializers.CharField(read_only=True, source="journey.end.name")
    bus_registration = serializers.CharField(read_only=True, source="bus_driver.bus.registration")
    class Meta:
        model = Schedule
        fields = ('id', 'time_start', 'time_end', 'journey', 'bus_driver', 'city_start', 'city_end', 'bus_registration')

class ScheduleCapacitySerializer(serializers.ModelSerializer):
    city_start = serializers.CharField(read_only=True, source="journey.start.name")
    city_end = serializers.CharField(read_only=True, source="journey.end.name")
    bus_registration = serializers.CharField(read_only=True, source="bus_driver.bus.registration")
    number_bus_seats = serializers.SerializerMethodField()
    number_ocuppied_seats = serializers.SerializerMethodField()
    ocuppied_percentage = serializers.SerializerMethodField()

    class Meta:
        model = Schedule
        fields = ('id', 'time_start', 'time_end', 'journey', 'city_start',
                  'city_end', 'bus_registration', 'number_bus_seats', 'number_ocuppied_seats',
                  'ocuppied_percentage')

    #atributo de porcentaje de ocupacion
    def get_ocuppied_percentage(self, obj):
        return obj.percentage

    #atributo numero de asientos
    def get_number_bus_seats(self, obj):
        return obj.bus_driver.bus.seat_bus.count()

    #atributo numero de asientos ocupados
    def get_number_ocuppied_seats(self, obj):
        return obj.ticket_schedule.count()