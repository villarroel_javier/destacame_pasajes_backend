# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

#modelo Ciudad
class City(models.Model):
    name = models.CharField(max_length=255, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

#modelo Trayecto
class Journey(models.Model):
    start = models.ForeignKey(City, related_name='city_start')
    end = models.ForeignKey(City, related_name='city_end')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    class Meta:
        unique_together = (("start", "end"),)

    #atributo para describir el trayecto
    def fullName(self):
        return "{} - {}".format(self.start.name, self.end.name)

#modelo Horario
class Schedule(models.Model):
    time_start = models.DateTimeField(null=False, blank=True)
    time_end = models.DateTimeField(null=False, blank=True)
    bus_driver = models.ForeignKey('bus.Bus_Driver', related_name='schedule_busdriver', on_delete=models.CASCADE)
    journey = models.ForeignKey(Journey, related_name='schedule_journey', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)