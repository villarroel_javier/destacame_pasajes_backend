# Backend Pasajes Destacame

Esta aplicación funciona como Backend de esta [aplicacion](https://bitbucket.org/villarroel_javier/destacame_pasajes_front/src)

## Prerequisitos
- Git https://es.atlassian.com/git/tutorials/install-git
- Python https://www.python.org/downloads/
- Pip https://pip.pypa.io/en/stable/installing/

## Clonar repositorio
```
git clone https://villarroel_javier@bitbucket.org/villarroel_javier/destacame_pasajes_backend.git
```

## Instalar dependencias del proyecto
```
cd destacame_pasajes_backend/pasajes/  
pip install -r ../requirements.txt  
```

## Migrar BD y crear usuario

Al momento de crear el usuario se recomienda las credenciales
- usuario: destacame
- password: destacame123
```
cd destacame_pasajes_backend/
python manage.py migrate
python manage.py createsuperuser
```

## Correr aplicación y crear usuario
```
cd destacame_pasajes_backend/
python manage.py runserver
```

## Consideraciones importantes

- La aplicación Backend supone que el Frontend se encuentra disponible
en la dirección http://localhost:8080/
- En caso de que no fuera así, es necesario modificar el archivo "settings.py", ubicado en la carpeta "pasajes"
- El apartado a modificar es el siguiente:
```
CORS_ORIGIN_WHITELIST = (
    'localhost:8080', #direccion del frontend
)
```

## Credenciales de acceso a ser usadas desde el Frontend
- usuario: destacame
- password: destacame123

En caso de haber elegido otras, es importante recordarlas para ser usadas en el Frontend